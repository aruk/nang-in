"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
class Command {
    constructor(name, params) {
        this.name = name;
        this.params = params;
    }
}
function main() {
    const filePath = 'input.txt';
    const commands = getCommandFromInputFile(filePath);
    commands.forEach(command => {
        logCommand(command);
        switch (command.name) {
            case 'add_book': {
                const [title, price, numberOfBooks] = command.params;
                console.log(`Added "${title}" to store.`);
                break;
            }
            default:
                return;
        }
        console.log('');
    });
}
main();
function getCommandFromInputFile(inputFile) {
    return fs_1.readFileSync(inputFile, 'utf-8')
        .split('\n')
        .map(line => line.split(' '))
        .map(([name, ...params]) => new Command(name, params));
}
function logCommand(command) {
    console.log(`> ${command.name} ${command.params.join(' ')}`);
}
