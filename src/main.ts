import { readFileSync } from 'fs';

class Command {
  constructor(public name: string, public params: string[]) {}
}

function main() {
  const filePath = 'input.txt';
  const commands = getCommandFromInputFile(filePath);

  commands.forEach(command => {
    logCommand(command);

    switch (command.name) {
      case 'add_book': {
        const [title, price, numberOfBooks] = command.params;

        console.log(`Added "${title}" to store.`);

        break;
      }

      default:
        return;
    }

    console.log('');
  });
}

main();

function getCommandFromInputFile(inputFile: string): Command[] {
  return readFileSync(inputFile, 'utf-8')
    .split('\n')
    .map(line => line.split(' '))
    .map(([name, ...params]) => new Command(name, params));
}

function logCommand(command: Command) {
  console.log(`> ${command.name} ${command.params.join(' ')}`);
}
